# 章节标签

# 表示文章

* 标题 h1-h6
* 章节 section
* 文章 article
* 段落 p
* 头部 header
* 脚部 footer
* 主要内容 main
* 旁支内容 aside
* 划分 div
* 版权标志 &copy;

</br>
</br>

# 代码
    <!DOCTYPE html>
    <html lang="zh-CN">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>第一次网页</title>
    </head>
    <body>
        <header>顶部广告</header>
        <div>
            <main>
            <h1>文章标题</h1>
            <section>
                <h2>第一章</h2>
                <p>这是一段话</p>
            </section>

            <section>
                <h2>第二章</h2>
                <p>这是一段话</p>
            </section>
            </main>
        </div>
        <footer>&copy; 版权声明</footer>
    </body>
    </html>
    </br>
    </br>

# 全局属性

* class  尽量不用id
* contenteditable 在网页中让用户可以自己输入
* hidden
* id
* style
* tabindex  用tab键选择，1-100依次顺序、0最后、-1永远不选择
* title 


# 内容标签

* ol+li 有序列表:
<pre>
        <ol>
            <li></li>
            <li></li>
        </ol>
</pre>
* ul+li 无序列表:
* <pre>
        <ul>
            <li></li>
            <li></li>
        </ul>
  </pre>
* dl+dt+dd  表述列表:
  <pre>
        <dl>
            <dt>电脑</dt>
            <dd>电脑很好玩</dd>
        </dl>
  </pre>

* pre 
* code 写代码
* hr 水平线
* br 换行
* a 链接
* em 表示语气上的强调
* strong 表示内容本身的重要性
* quote 一行的引用，没什么区别
* blockquote 块的引用，第二行会缩进两个字符
  
</br>
</br>

# HTML重点标签
</br>

## 用输入地址的方式打开HTML

</br>

### 第一种方式
</br>

1.yarn global add http-server  安装插件</br>
2.http-server . -c-1</br>
3.输入地址+文件名.html

### 第二种方式

1.yarn global add parcel 安装插件</br>
2.parcel 文件名.html
</br>
</br>
</br>

## a标签
* href  超链接 (href="http://google.com")
* target  指定窗口打开页面 (target="_blank")
* rel=noopener


### javascript伪协议
    <a href="javascript:alert(1)">javascript伪协议</a>

### 做一个什么都不做的a标签
    <a href="javascritp:;">查看</a>

### 跳转到指定的标签
    <a href="#xxx">查看xxx</a>

    意思就是跳转到id=xxx的地方

### 发邮箱
    <a href="mailto:fangyuan.leslie@qq.com">点击发邮箱</a>

### 打电话
    <a href="tel:17628486456">打电话给我</a>

</br>
</br>
</br>

## target
    _blank 在空白页面打开
    _top 在顶级窗口打开（最上面的窗口）
    _parent 在当前链接的窗口iframe的上一级窗口打开
    _self 自己的窗口打开（默认）

    target="xxx" 在xxx页面打开，只要target="xxx"所有链接都会在这一个页面打开

## iframe（相当于网页中创建一块）
    name=xxx

## table 表格
    <table>
        <thead> 头部
            <tr>
                <th>英语</th>
                <th>翻译</th>
            </tr>
        </thead>
        <tbody> 身体
            <tr>
                <td>hyper</td>
                <td>超级</td>
            </tr>
            <tr>
                <td>target</td>
                <td>目标</td>
            </tr>
            <tr>
                <td>reference</td>
                <td>引用</td>
            </tr>
        </tbody> 
        <tfoot> 尾部
            <tr>
                <td>空</td>
                <td>空</td>
            </tr>
        </tfoot>
    </table>


    table-layout:表格单元格会适应表格当中的文字
    border-collapse:合并单元格的间距
    border-spacing:用户自己输入单元格的间距

## img标签

</br>

### 作用
发出get请求，展示一张图片
### 属性
    alt 图片加载出错的时候显示文字
    height 高度（只写高度高度会自适应）
    width 宽度 （同理）
### 事件
    onload 加载成功
    onerror 加载失败
### 图片响应式
    <style>
      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
      }
      img {
        max-width: 100%;
      }
    </style>
## form 标签

### 作用
    发get或POST请求，然后刷新页面

### 属性
    action 当前页面跳转到给的地址
    autocomplete 给用户选择
    method 选择用get或POST
    target 吧哪个页面跳转到给的值(与a标签一样)
### 事件
    onsubmit（当用户点击提交触发的事件）

### input标签
    <input type="text" required />  文本框
    required 验证器，（必须输入，不然无法提交）

    <input type="color" />  用户自选颜色
    <input type="password" />  密码
    <input name="gemder" type="radio" /> 男 <input name="gemder" type="radio" /> 女 //单选
    <input name="hobby" type="checkbox" /> 唱 <input name="hobby" type="checkbox" /> 跳   //多选
    <input type="file" /> 上传一个文件
    <input type="file" multiple /> 上传多个文件
    <input type="hidden" /> 给js填id或者字符串之类的

    <textarea style="resize: none" width: ; height: ;></textarea> 多行文本框，默认右下角可以调节大小，也可以加上样式固定大小

    <option value="">- 请选择 -</option>
    <option value="1">星期一</option>
    <option value="2">星期二</option>

## 事件
* onchange 用户输入（改变）触发
* onfocus 用户鼠标放在上面的事件
* onblur 用户鼠标移出来的事件

# 注意事项
* 一般不监听input的click事件
* form里面的input要有name
* form里面要放一个type=submit才能触发submit事件（submit是点击事件）